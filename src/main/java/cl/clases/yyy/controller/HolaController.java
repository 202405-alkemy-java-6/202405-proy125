package cl.clases.yyy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HolaController {

	@GetMapping("/hola")
	public String hola(Model modelo) {

		modelo.addAttribute("mensaje", "hola desde thymeleaf");
		return "hola";
	}
	

	@GetMapping("/chao")
	public String chaoyyyyy(Model modelo) {

		modelo.addAttribute("mensaje", "CHAO! desde thymeleaf");
		return "chaoxxxxxx";
	}
}
